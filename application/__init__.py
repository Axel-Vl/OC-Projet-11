from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS
from dotenv import load_dotenv

load_dotenv()

db = SQLAlchemy()
migrate = Migrate()
auth = HTTPBasicAuth()
login = LoginManager()


def create_app(config_filename=None):
    """Initialize the core application"""
    app = Flask(__name__, instance_relative_config=True)
    CORS(app)
    app.config.from_pyfile(config_filename)

    # Initialize plugins
    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)

    with app.app_context():
        from . import routes, models, forms

        db.create_all()

        return app
