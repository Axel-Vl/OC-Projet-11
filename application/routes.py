from flask import request, jsonify, url_for, g, abort
from flask import current_app as app
from application.models import User, Messages
from application import research
from application import db, auth


@app.route('/search_request', methods=['GET', 'POST'])
def search_request():
    print(request.json)
    user_request = request.json['user_input']

    r = research.Research(user_request)
    r.main()
    response = {
        'lat': r.lat,
        'lng': r.lng,
        'formatted_address': r.formatted_address,
        'wiki_summary': r.wiki_summary,
        'wiki_url': r.wiki_url,
        'grandpy_response': r.grandpy_response,
        'status': r.status,
    }

    return jsonify(response)


@app.route('/user_search_request', methods=['GET', 'POST'])
@auth.login_required
def user_search_request():
    user_request = request.json['user_input']

    r = research.Research(user_request)
    r.main()
    response = {
        'lat': r.lat,
        'lng': r.lng,
        'formatted_address': r.formatted_address,
        'wiki_summary': r.wiki_summary,
        'wiki_url': r.wiki_url,
        'grandpy_response': r.grandpy_response,
        'status': r.status,
    }
    user = User.query.filter_by(username=auth.username()).first()
    if user is None:
        user = User.verify_auth_token(auth.username())
    message = Messages(
            user_id=user.id,
            user_request=user_request,
            response_lat=r.lat,
            response_lng=r.lng,
            response_formatted_address=r.formatted_address,
            response_wiki_summary=r.wiki_summary,
            response_wiki_url=r.wiki_url,
            response_grandpy_response=r.grandpy_response
    )
    db.session.add(message)
    db.session.commit()

    return jsonify(response)


@app.route('/user_history', methods=['GET'])
@auth.login_required
def user_search_history():
    user = User.query.filter_by(username=auth.username()).first()
    if user is None:
        user = User.verify_auth_token(auth.username())
    messages = Messages.query.filter_by(user_id=user.id)

    user_history = []

    for message in messages.all():
        formated_message = {
            "datetime": message.datetime,
            "user_request": message.user_request,
            "response_lat": message.response_lat,
            "response_lng": message.response_lng,
            "response_formatted_address": message.response_formatted_address,
            "response_wiki_summary": message.response_wiki_summary,
            "response_wiki_url": message.response_wiki_url,
            "response_grandpy_response": message.response_grandpy_response
        }
        user_history.append(formated_message)

    return jsonify(user_history)


@app.route('/create_user', methods=['POST'])
def create_user():
    username = request.json.get('username')
    password = request.json.get('password')
    if username is None or password is None:
        abort(400)  # missing arguments
    if User.query.filter_by(username=username).first() is not None:
        abort(403)  # existing user
    user = User(username=username)
    user.set_password(password)
    db.session.add(user)
    db.session.commit()
    return (jsonify({'username': user.username}), 201,
            {'Location': url_for('get_user', id=user.id, _external=True)})


@app.route('/users/<int:id>')
def get_user(id):
    user = User.query.get(id)
    if not user:
        abort(400)
    return jsonify({'username': user.username})


@app.route('/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token()
    return jsonify({'token': token.decode('ascii')})


@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True
