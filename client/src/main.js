import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import store from "./store";
import * as VueGoogleMaps from "vue2-google-maps"

if (process.env.NODE_ENV === 'development') {
  Vue.use(VueGoogleMaps, {
    load: {
      key: process.env.VUE_APP_DEV_GOOGLE_CLOUD_TOKEN,
      libraries: "places"
    }
  });
}
Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount("#app");
