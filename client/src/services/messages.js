import axios from 'axios';
import base64 from "./base64";

export default {
  getUserHistory(userToken) {
    const url = "http://localhost:5000/user_history";
    const headers = {
      Authorization: `Basic ${base64.encode(`${userToken}:`)}`
    };
    return axios.get(url, {headers: headers})
  },
  searchRequest(userInput) {
    const url = "http://localhost:5000/search_request";
    const data = {user_input: userInput};

    return axios.post(url, data)
  },
  userSearchRequest(userInput, userToken) {
    const url = "http://localhost:5000/user_search_request";
    const data = {user_input: userInput};
    const headers = {
      Authorization: `Basic ${base64.encode(`${userToken}:`)}`
    };

    return axios.post(url, data, {headers: headers})
  },

}