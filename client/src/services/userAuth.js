import axios from "axios";
import base64 from "./base64";

export default {
  createUser(username, password) {
    const url = "http://localhost:5000/create_user";
    const data = {username: username, password: password};
    return axios.post(url, data);
  },
  loginUser(username, password) {
    const url = "http://localhost:5000/token";
    const headers = {
      Authorization: `Basic ${base64.encode(`${username}:${password}`)}`
    };
    return axios.get(url, {headers: headers});
  }
};

