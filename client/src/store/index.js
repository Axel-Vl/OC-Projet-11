import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    connexionToken: undefined,
    connexionTokenValidity: undefined
  },
  mutations: {
    changeConnexionToken(state, newConnexionToken) {
      state.connexionToken = newConnexionToken;
    },
    changeConnexionTokenValidity(state, newConnexionTokenValidity) {
      state.connexionToken = newConnexionTokenValidity;
    }
  },
  actions: {},
  getters: {
    isLoggedIn(state) {
      return state.connexionToken !== undefined;
    }
  },
  modules: {}
});
