import os
from dotenv import load_dotenv


basedir = os.path.abspath(os.path.dirname(__file__))


class Config:

    ENV = os.environ.get('ENV')
    if ENV is None:
        ENV = 'DEV'

    if ENV == 'DEV':
        SECRET_KEY = os.environ.get('SECRET_KEY')
        GOOGLE_CLOUD_TOKEN_BACK = os.environ.get('GOOGLE_CLOUD_TOKEN_DEV')
        GOOGLE_CLOUD_TOKEN_FRONT = os.environ.get('GOOGLE_CLOUD_TOKEN_DEV')
        SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'application.db')
        SQLALCHEMY_COMMIT_ON_TEARDOWN = True
        SQLALCHEMY_TRACK_MODIFICATIONS = False

    elif ENV == 'PROD':
        SECRET_KEY = os.environ.get('SECRET_KEY')
        GOOGLE_CLOUD_TOKEN_BACK = os.environ.get('GOOGLE_CLOUD_TOKEN_BACK')
        GOOGLE_CLOUD_TOKEN_FRONT = os.environ.get('GOOGLE_CLOUD_TOKEN_FRONT')
        SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
        SQLALCHEMY_COMMIT_ON_TEARDOWN = True
        SQLALCHEMY_TRACK_MODIFICATIONS = False
