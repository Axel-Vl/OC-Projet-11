import pytest
from application import create_app, db
from application.models import User, Messages


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app('flask_test.cfg')

    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()


@pytest.fixture(scope='module')
def init_database():
    db.create_all()

    user = User(username="test_init")
    user.set_password("test_init")
    db.session.add(user)

    messages = Messages(user_id=1, user_request="toulouse")

    db.session.commit()

    yield db

    db.drop_all()
