def test_create_user(test_client, init_database):
    from application.models import User

    body = {"username": "test", "password": "test"}

    response = test_client.post("/create_user", json=body)
    assert User.query.filter_by(username="test").first() is not None


def test_authentication_and_session_token(test_client, init_database):
    from application import db
    from application.models import User

    response = test_client.get(
            "/token", headers={"Authorization": "Basic dGVzdF9pbml0OnRlc3RfaW5pdA=="}
    )
    token = response.json["token"]

    assert response.status_code == 200
    assert token is not None


def test_wrong_credentials(test_client, init_database):
    response = test_client.get(
            "/token", headers={"Authorization": "Basic dGVzdDp0ZXN0MTIzNA=="}
    )

    assert response.status_code == 401


def test_basic_research(test_client, init_database):
    body = {"user_input": "Toulouse"}

    response = test_client.post("/search_request", json=body)
    assert response.status_code == 200


def test_logged_in_research(test_client, init_database):
    from application.models import Messages

    body = {"user_input": "Toulouse"}

    response = test_client.post("/user_search_request", json=body, headers={
        "Authorization": "Basic dGVzdF9pbml0OnRlc3RfaW5pdA=="})

    assert response.status_code == 200
    assert Messages.query.first().user_id == 1
    assert Messages.query.first().user_request == "Toulouse"


def test_not_logged_in_research(test_client, init_database):
    from application.models import Messages

    body = {"user_input": "Toulouse"}

    response = test_client.post("/user_search_request", json=body)

    assert response.status_code == 401


def test_user_history(test_client, init_database):
    from application.models import Messages

    response = test_client.get('user_history', headers={
        "Authorization": "Basic dGVzdF9pbml0OnRlc3RfaW5pdA=="})

    assert response.status_code == 200
