from application import create_app

app = create_app('flask_dev.cfg')

if __name__ == "__main__":
    app.run(host='0.0.0.0')